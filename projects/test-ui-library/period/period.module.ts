import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {PeriodComponent} from './period.component';



@NgModule({
  declarations: [PeriodComponent],
  imports: [
    CommonModule
  ]
})
export class PeriodModule { }
